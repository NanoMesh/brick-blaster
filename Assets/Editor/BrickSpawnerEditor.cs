using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BrickSpawner))]

public class BrickSpawnerEditor : Editor
{

     public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        BrickSpawner brickSpawner = (BrickSpawner)target;
        if(GUILayout.Button("Spawn Bricks"))
        {
            brickSpawner.SpawnBricks();
        }
    }

}
