using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
    //static variables
	public static GameManager localGameManager;
    public static GameState gameState;
    public static GameMode gameMode;
    public static int NumberOfLives;
    public static int NumberOfBalls;
    public static int Score;
    public static int BrickNum;
    public static int LVLNumber;
    public static int WaveNumber;
    
    //public variables
    public enum GameState{InMenu,Firing,Bouncing,Pause,GameOver,Victory};
	public GameState localGamestate;
	public bool useLocalGamestate;

	public enum GameMode{Normal, Endless};
    public GameMode localGameMode;
    
	public enum PlayerBatType{Classic, Modern, Rotating};
	public PlayerBatType localPlayerBatType;
    
    
	public static void ResetStats() 
    {
    Debug.Log("Stats Resetted");        
	NumberOfLives = 3;
    NumberOfBalls = 0;
    Score = 0;
    BrickNum = 0;
    WaveNumber=1;
	}
    
    //Substracst Live and check for status of the game
    public static void SubstractLive()
    {
    NumberOfBalls--;        
    if(NumberOfBalls <= 0)
        {
        NumberOfLives--;
        if(NumberOfLives > 0)
        {
            gameState = GameState.Firing;
        }
        Debug.Log("Live Removed");
        }
    CheckForGameStatus();
    }
    
    //Checks for Game over or victory
    public static void CheckForGameStatus()
    {
        if(gameMode != GameMode.Endless)
        {
        if(BrickNum <= 0)
            {
                gameState = GameState.Victory;
                Debug.Log("Victory");
            }
        }else if(BrickNum <= 0 && gameMode == GameMode.Endless)
        {
			localGameManager.StartCoroutine ("DelayNextSpawn", 1);
        }
        
        if(NumberOfLives <= 0)
        {
            gameState = GameState.GameOver;
            Debug.Log("GameOver");
        }
    }
    
	public void NextWave()
	{
		Debug.Log("Next wave");
		WaveNumber++;
		LVLNumber++;
		GameObject.Find("BrickSpawner").SendMessage("SpawnBricks");
		GameObject.Find ("WaveText").GetComponent<Text> ().text = "WAVE \n " + WaveNumber.ToString ();
		GameObject.Find ("WaveText").GetComponent<Animator> ().SetTrigger ("AnimWave");
	}

	public void SetLVLNumber(int LVLnum)
	{
		LVLNumber = LVLnum;
	}

    public void NextLevel()
    {
        Debug.Log("Next Level");
        LVLNumber++;
        GameObject[] ballsAlive = GameObject.FindGameObjectsWithTag("Ball");
        if(ballsAlive != null)
            {
                for(int i=0;i<ballsAlive.Length;i++)
                {
                    DestroyImmediate(ballsAlive[i]);
                }
            }
        GameObject.Find("BrickSpawner").SendMessage("SpawnBricks");
        gameState = GameState.Firing;
    }

	//Set bat type with button
	public void SetLocalBatType(string batTypeName)
	{
		switch(batTypeName)
		{
		case "Classic":
			localPlayerBatType = PlayerBatType.Classic;
			break;
		case "Modern":
			localPlayerBatType = PlayerBatType.Modern;
			break;
		case "Rotating":
			localPlayerBatType = PlayerBatType.Rotating;
			break;
		default:
			Debug.Log("Chybka v SetBatType GameManager");
			break;
		}
	}

	//Set playerBatType in Bat Script
	public void SetBatType(string batTypeName)
	{
		switch(batTypeName)
		{
		case "Classic":
			GameObject.Find ("PlayerBat").GetComponent<Bat> ().SetBatType ("Classic");

		break;
		case "Modern":
			GameObject.Find ("PlayerBat").GetComponent<Bat> ().SetBatType ("Modern");

		break;
		case "Rotating":
			GameObject.Find ("PlayerBat").GetComponent<Bat> ().SetBatType ("Rotating");
		break;
		default:
			Debug.Log("Chybka v SetBatType GameManager");
		break;
		}
	}
	void Start()
    {
        gameMode = localGameMode;
		if(useLocalGamestate)
		{
			gameState = localGamestate;
			GameObject.Find("BrickSpawner").GetComponent<BrickSpawner>().SpawnBricks();
			SetBatType (localPlayerBatType.ToString ());
		}else
		{
			gameState = GameState.InMenu;	
		}


        ResetStats();
    }
	void OnEnable()
	{

		if(localGameManager == null)
		{
			localGameManager = this;
		}else
		{
			Destroy(gameObject);
			return;
		}
	}
	void Update () 
    {
        if(Input.GetKeyDown(KeyCode.N))
        {
            NextLevel();

        }
		if(Input.GetKeyDown(KeyCode.N))
		{
			NextWave();

		}
	}

	IEnumerator DelayNextSpawn(int type)
	{
		yield return new WaitForSeconds(1f);
		if (type == 0) 
		{
			NextLevel();
		} else if (type == 1)
		{
			NextWave ();
		}

	}
}
