﻿using UnityEngine;
using System.Collections;

public class PowerUpObject : MonoBehaviour
{ 
	public Vector2 moveSpeed;
    public enum PowerUpType { None, BiggerBat, SmallerBat, FasterBall, SlowerBall };
    public PowerUpType powerUpType;
	private Rigidbody2D rigidB;

	void Start()
	{
		rigidB = gameObject.GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
		if (GameManager.gameState == GameManager.GameState.Bouncing) 
		{
			rigidB.velocity = moveSpeed;
		}else
		{
			rigidB.velocity = new Vector2(Mathf.Lerp(rigidB.velocity.x,0,.1f),Mathf.Lerp(rigidB.velocity.y,0,.1f));
		}

	}

    void OnTriggerEnter2D(Collider2D col)
    {
    if(col.gameObject.tag == "Player")
        {
            switch (powerUpType)
            {
                case PowerUpType.BiggerBat:
                    col.gameObject.SendMessage("ApplyPowerUp","BiggerBat");
                    Destroy(gameObject);
                    break;
                case PowerUpType.SmallerBat:
                    col.gameObject.SendMessage("ApplyPowerUp", "SmallerBat");
                    Destroy(gameObject);
                    break;
                default:
                    Debug.Log("Chybka v PowerUpObjectu");
                    break;

            }

        }
    if (col.gameObject.tag == "DeathZone")
        {
            Destroy(gameObject);

        }
    }
}
