using UnityEngine;
using System.Collections;

public class GridMover : MonoBehaviour 
{
    public Material mat;
    public float moveSpeed;
	void Start () 
    {
	mat = this.gameObject.GetComponent<Renderer>().material;
	}
	

    void Update () 
    {
        mat.mainTextureOffset -= new Vector2(0,moveSpeed * Time.deltaTime);
        if(mat.mainTextureOffset.y <= -1)
        {
            mat.mainTextureOffset = new Vector2(0,0);
        }
	}
}
