using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour 
{
    //public variables
    public float ballSpeed = 5f;
    public GameObject hitEffect;

    //private variables
    private Rigidbody2D rigidBall;
    private Camera cam;
    private Animator ballAnim;
	private float origSpeed;

	void Start () 
    {
	   rigidBall = gameObject.GetComponent<Rigidbody2D>();
       cam = Camera.main;
        ballAnim = gameObject.GetComponent<Animator>();
		origSpeed = ballSpeed;
	}
	
	void FixedUpdate () 
    {	
		if(GameManager.WaveNumber > 10 && GameManager.WaveNumber < 20 )
		{
			ballSpeed = origSpeed + 1;
		}else if(GameManager.WaveNumber > 20 && GameManager.WaveNumber < 30 )
		{
			ballSpeed = origSpeed + 2;
		}
		else if(GameManager.WaveNumber > 30 && GameManager.WaveNumber < 40 )
		{
			ballSpeed = origSpeed + 3;
		}
		else if(GameManager.WaveNumber > 40 && GameManager.WaveNumber < 50 )
		{
			ballSpeed = origSpeed + 4;
		}
		else if(GameManager.WaveNumber > 50 && GameManager.WaveNumber < 60 )
		{
			ballSpeed = origSpeed + 5;
		}
        //Move ball only when there is no victory
        if(GameManager.gameState != GameManager.GameState.Victory)
        {
	       rigidBall.velocity = rigidBall.velocity.normalized * ballSpeed;    
        }else
        {
          StartCoroutine(DelayedDestruction());
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        cam.GetComponent<CameraShake>().Shake();
        ballAnim.SetTrigger("Hit");
        SpawnHitEffect(col.contacts[0].normal,col.contacts[0].point);

        if(col.gameObject.tag == "DeathZone")
        {
            
            //Kill The Ball
            Debug.Log("Ball has entered DeathZone and died");
            GameManager.SubstractLive();
            Destroy(gameObject);
        }
        
        if(col.gameObject.tag == "Brick")
        {
         //Kill the Brick
         col.gameObject.SendMessage("HitBrick");
        }
        
        if(col.gameObject.tag == "Wall")
        {
            //Pushes the ball to the left when is moving purely up and down when hitting the wall.
            if(rigidBall.velocity.x > -0.1f && rigidBall.velocity.x < 0.1f)
            {
                Debug.Log("Ball Pushed Left");
                rigidBall.AddForce(new Vector2(-0.01f,0f),ForceMode2D.Impulse);
            }
 
        
        //Pushes the ball to the bottom when is moving purely left and right.
            if(rigidBall.velocity.y > -0.1f && rigidBall.velocity.y < 0.1f)
            {
            if(col.contacts[0].normal.x <= -0.98f || col.contacts[0].normal.y <= -0.98f)
                {
                Debug.Log("Ball Pushed Down");
                rigidBall.AddForce(new Vector2(0f,-0.01f),ForceMode2D.Impulse);
                }
                else if(col.contacts[0].normal.x >= 0.98f || col.contacts[0].normal.y >= 0.98f)
                {
                Debug.Log("Ball Pushed Up");
                rigidBall.AddForce(new Vector2(0f, 0.01f),ForceMode2D.Impulse);
                }
            }
        }
        
        
    }
    
    void SpawnHitEffect(Vector2 normalHit,Vector2 pos)
    {
        float effectRotation = 90f;
        if(normalHit.x > 0.5f)
        {
            effectRotation = 270f;
        }else if(normalHit.x < -0.5f)
        {
            effectRotation = 90f;
        }
        else if (normalHit.y > 0.5f)
        {
            effectRotation = 0f;
        }
        else if (normalHit.y < -0.5f)
        {
            effectRotation = 180f;
        }



        GameObject effectClone = hitEffect;
        effectClone = Instantiate(hitEffect,pos,transform.rotation) as GameObject;
        effectClone.transform.localRotation = Quaternion.AngleAxis(effectRotation, Vector3.forward);
    }

    IEnumerator DelayedDestruction()
    {
    Debug.Log("Ball is getting destroyed");
    rigidBall.velocity = new Vector2(Mathf.Lerp(rigidBall.velocity.x,0f,.05f),Mathf.Lerp(rigidBall.velocity.y,0f, .05f));
    yield return new WaitForSeconds(2f);
        ballAnim.SetTrigger("Hit");
        yield return new WaitForSeconds(.06f);
        Destroy(gameObject);
    }
}
