using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "MapBrickList", menuName = "BrickMapList", order = 1)]

public class BrickScriptableObjectList : ScriptableObject
{
    //Creates list of BrickClass objects for spawning
    
    public List<BrickClass> BrickList;

}
