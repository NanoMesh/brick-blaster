using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour 
{
	public static UIManager UiManagerInstance;

	public enum UiState{MainMenu,OptionsMenu,EndlessSelectMenu,CampaignSelectMenu,Credits,LoadingScreen,PlayingScreen};
	public static UiState uiState;
	public UiState localUiState;

	public GameObject[] MenuPanel;
	public GameObject[] SubMenuPanels;



    public Text scoreText;
    public Text livesText;
    public Text ballsText;
    public Text waveText;
    public Text lvlText;
    public Text brickNum;
    public Text GameStatus;
    public Text GameModus;

	private Scene curScene;

	void OnEnable()
	{
		//Checking for duplicate managers
		DontDestroyOnLoad(gameObject);

		if(UiManagerInstance == null)
		{
			UiManagerInstance = this;
		}else
		{
			Destroy(gameObject);
			return;
		}

		uiState = localUiState;
		CheckUiState ();

	}
		
	public static void CheckUiState()
	{
		switch (uiState)
		{
		case UiState.MainMenu:
			UiManagerInstance.SwitchUiScreen("MainMenu");
		break;
		case UiState.OptionsMenu:
			UiManagerInstance.SwitchUiScreen("OptionsMenu");
		break;
		case UiState.EndlessSelectMenu:
			UiManagerInstance.SwitchUiScreen ("EndlessSelectMenu");
		break;
		case UiState.CampaignSelectMenu:
			UiManagerInstance.SwitchUiScreen ("CampaignSelectMenu");
		break;
		case UiState.Credits:
			UiManagerInstance.SwitchUiScreen ("Credits");
		break;
		case UiState.LoadingScreen:
			UiManagerInstance.SwitchUiScreen ("LoadingScreen");
		break;
		case UiState.PlayingScreen:
			UiManagerInstance.SwitchUiScreen ("PlayingScreen");
		break;
		default:
			Debug.Log("Chybka v UiManageru");
		break;

		}
	}
	
	void Update () 
    {
	   scoreText.text = ("SCORE: " + GameManager.Score.ToString());
       livesText.text = ("LIVES: " + GameManager.NumberOfLives.ToString());
       ballsText.text = ("BALLS: " + GameManager.NumberOfBalls.ToString());
       waveText.text = ("WAVE: " + GameManager.WaveNumber.ToString());
       lvlText.text = ("LVL: " + GameManager.LVLNumber.ToString());
       brickNum.text = ("Bricks: " + GameManager.BrickNum.ToString());
       GameStatus.text = ("Gamestatus: " + GameManager.gameState.ToString());
       GameModus.text = ("Gamemode: " + GameManager.gameMode.ToString());

    }
	//Aktivovane tlacitkem
	public void SwitchUiScreen(string screenName)
	{
		switch(screenName)
		{
		case "MainMenu":
			uiState = UiState.MainMenu;
			StartCoroutine ("DelayedDesablition", 0);
		break;
		case "OptionsMenu":
			uiState = UiState.OptionsMenu;
			StartCoroutine ("DelayedDesablition", 1);
		break;
		case "EndlessSelectMenu":
			uiState = UiState.EndlessSelectMenu;
			StartCoroutine ("DelayedDesablition", 2);
		break;
		case "CampaignSelectMenu":
			uiState = UiState.CampaignSelectMenu;
			StartCoroutine ("DelayedDesablition", 3);
		break;
		case "Credits":
			uiState = UiState.Credits;
			StartCoroutine ("DelayedDesablition", 4);
		break;
		case "LoadingScreen":
			uiState = UiState.LoadingScreen;
			StartCoroutine ("DelayedDesablition", 5);
		break;
		case "PlayingScreen":
			uiState = UiState.PlayingScreen;
			StartCoroutine ("DelayedDesablition", 6);
		break;
		default:
			Debug.Log("Chybka v UiManageru SwitchUiScreen");
		break;
		}

	}

	IEnumerator DelayedDesablition(int menuObjInt)
	{
		Debug.Log ("Switching Screens"+menuObjInt.ToString());

		for (int i = 0; i < MenuPanel.Length; i++) 
		{
			if(MenuPanel[i].activeSelf == true)
			{
			MenuPanel[i].GetComponent<Animator>().SetBool ("On", false);
			}
		}
		yield return new WaitForSeconds(1f);
		for (int i = 0; i < MenuPanel.Length; i++) 
		{
			if (MenuPanel [i].activeSelf == true) 
			{
			MenuPanel [i].SetActive (false);
			}
		}
		MenuPanel[menuObjInt].SetActive (true);
		MenuPanel[menuObjInt].GetComponent<Animator>().SetBool ("On", true);
	}
	public void SwitchSubMenu(int subMenuObjInt)
	{
		for (int i = 0; i < SubMenuPanels.Length; i++) 
		{
				SubMenuPanels [i].SetActive (false);
		}
		SubMenuPanels[subMenuObjInt].SetActive (true);
	}
	public void SetGameMode(string GameModeName)
	{
		if(GameModeName == "Normal")
		{
		GameManager.gameMode = GameManager.GameMode.Normal;
		}else if(GameModeName == "Endless")
		{
			GameManager.gameMode = GameManager.GameMode.Endless;	
		}
	}
	//Button to activate loading
	public void StartLevel(string levelName)
	{
		StopCoroutine("loadLevelWaiter");    
		StartCoroutine("loadLevelWaiter", levelName);
	}

	//Load level with loading screen
	IEnumerator loadLevelWaiter(string levelName)
	{
		Debug.Log("Start Loading");
		curScene = SceneManager.GetActiveScene();
		//Show loading screen
		SwitchUiScreen("LoadingScreen");

		yield return new WaitForSeconds(2f);

		AsyncOperation async = SceneManager.LoadSceneAsync(levelName,LoadSceneMode.Additive);

		Debug.Log(async.progress);

		yield return async;

		//Moves menu object to loaded level and unloads previous one.
		//SceneManager.MoveGameObjectToScene(this.gameObject, SceneManager.GetSceneByName(levelName));
		SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelName));

		yield return new WaitForSeconds(2f);

		SceneManager.UnloadScene(curScene.name);

		curScene = SceneManager.GetActiveScene();

		yield return new WaitForSeconds(2f);

		//Hide loading Screen
		SwitchUiScreen("PlayingScreen");

		yield return new WaitForSeconds(2f);

		GameManager.gameState = GameManager.GameState.Firing;
		GameObject.Find("BrickSpawner").GetComponent<BrickSpawner>().SpawnBricks();
		GameManager.localGameManager.SetBatType (GameManager.localGameManager.localPlayerBatType.ToString ());

		Debug.Log("Loading Done");

	}
}
