﻿using UnityEngine;
using System.Collections;

public class AutoDestructSequence : MonoBehaviour
{
    public float DestructionTime;

	void Start ()
    {
	
	}
	
	void Update ()
    {
        DestructionTime -= Time.deltaTime;
        if(DestructionTime <= 0)
        {
            Destroy(gameObject);
        }
	}
}
