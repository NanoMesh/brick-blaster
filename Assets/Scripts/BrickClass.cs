using UnityEngine;
using System.Collections;

[System.Serializable]
public class BrickClass 
{
    //BrickClass containing informations for spawning
    public float BrickPosX;
    public float BrickPosY;
    public float BrickRotation;
    public GameObject BrickPrefab;
}
