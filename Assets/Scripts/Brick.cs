using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour 
{
    //public variables
    public enum BrickType{Normal,Heavy,MultiBall,ExtraLife,Spawner};
    public BrickType brickType;
    public int BrickHP;
    public Rigidbody2D ball;
    public Rigidbody2D powerupObject;
    public int scoreAmount;
    
    void HitBrick()
    {
        switch (brickType)
        {
            case BrickType.Normal:
                    ApplyDamage();
                break;
            case BrickType.Heavy:
                    ApplyDamage();
                break;    
            case BrickType.MultiBall:
                    ApplyDamage();
                break;    
            case BrickType.ExtraLife:
                    ApplyDamage();
                break;
            case BrickType.Spawner:
                ApplyDamage();
                break;   
            default:
                    Debug.Log("Chybka v Bricku");
                break;
        }    
    }
    
    void ApplyDamage()
    {
        Debug.Log("Brick has been Damaged");
        BrickHP--;
        transform.parent.gameObject.GetComponent<Animator>().SetTrigger("Hit");
        if (BrickHP <= 0)
        {
            GameManager.BrickNum--;
            GameManager.Score += scoreAmount;
            GameManager.CheckForGameStatus();
            
         if(brickType == BrickType.ExtraLife)
            {
                GameManager.NumberOfLives++;
                Debug.Log(GameManager.NumberOfLives);
            }

        if(brickType == BrickType.MultiBall)
            {
                BrickMultiBall();
            }
        if (brickType == BrickType.Spawner)
            {
                SpawnPowerUp();
            }
            
            StartCoroutine(DelayedDestruction());
        }
    }

    IEnumerator DelayedDestruction()
    {
        this.gameObject.GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(.11f);
		Destroy(transform.parent.gameObject);
    }
    void BrickMultiBall()
    {
        GameManager.NumberOfBalls++;
        gameObject.GetComponent<Collider2D>().enabled = false;
        Rigidbody2D clone = Instantiate(ball,transform.position,transform.rotation) as Rigidbody2D;
        clone.AddForce(Vector2.left);
    }

    void SpawnPowerUp()
    {
        gameObject.GetComponent<Collider2D>().enabled = false;
        Rigidbody2D clone = Instantiate(powerupObject, transform.position, transform.rotation) as Rigidbody2D;
        clone.AddForce(Vector2.left*2,ForceMode2D.Impulse);
    }
}
