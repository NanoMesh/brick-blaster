using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour 
{
    //Public fvariables
    public float ShakeStrength = 0.1f;
    public float ShakeDuration = 0.5f;
    public GameObject cameraTarget;

    //Private varibles
    private Vector3 initialPos;
    private bool isShaking = false;
    private float localShakeDuration;
    private Vector3 refSpeed = Vector3.zero;

    void Start()
    {
        localShakeDuration = ShakeDuration;
        initialPos = transform.localPosition;
    }
	
    public void Shake()
    {
       
       isShaking = true;
    }

	void LateUpdate () 
    {


        transform.position = Vector3.SmoothDamp(transform.position, cameraTarget.transform.position, ref refSpeed, .5f);

       if (isShaking && ShakeDuration > 0)
       {
       transform.localPosition = initialPos + Random.insideUnitSphere*ShakeStrength;
       ShakeDuration -= Time.deltaTime;
       }else
       {
       isShaking = false;
       ShakeDuration = localShakeDuration;
       }
	}
}
