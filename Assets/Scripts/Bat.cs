using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bat : MonoBehaviour 
{
    //public variables
    public enum BatType{Classic, Modern, Rotating};
    public BatType batType;
    
    public float batSpeed = 5f;
    public float batTurnSpeed = 5f;

    public GameObject BatParent;
    
    public Animator batAnimator;

    public int ResetPowerUpTime;
    //Turning stuff
    public bool TurnLimiting;
    public float rotationAngle;
    public float turnLimitLeft;
    public float turnLimitRight;
    //Colliders
    public Collider2D NormalBatCollider;
    public Collider2D BiggerBatCollider;
    public Collider2D SmallerBatCollider;

    
    public Rigidbody2D rigidBat;
    public bool isPowered = false;
    public Text text;
    public Animator textAnimator;

	public GameObject blockZone;


	public void SetBatType (string batTypeName) 
    {
		switch(batTypeName)
		{
		case "Classic":
			batType = BatType.Classic;
			blockZone.SetActive (false);
		break;
		case "Modern":
			batType = BatType.Modern;
			blockZone.SetActive (true);
		break;
		case "Rotating":
			batType = BatType.Rotating;
			blockZone.SetActive (false);
		break;
		default:
			Debug.Log("Chybka v SetBatType Bat");
		break;
		}
	}
    void FixedUpdate () 
    {
        if(GameManager.gameState == GameManager.GameState.Bouncing)
        {
            //Check which bat type is being used with default one being classic up and down
            switch(batType)
            {
                case BatType.Classic:
                        ClassicBatMovement();
                    break;
                case BatType.Modern:
                        ModernBatMovement();
                    break;
                case BatType.Rotating:
                        RotatingBatMovement();
                    break;
                default:
                        ClassicBatMovement();
                    break;
            }
        }else
        {
           rigidBat.velocity = new Vector2(0,0); 
        }
        
	   
	}
    //Moves bat up and down
    void ClassicBatMovement()
    {
       rigidBat.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;

       rigidBat.velocity = new Vector2(0,Input.GetAxis("Vertical")*batSpeed); 

    }
    //Moves bat up, down, right,left
    void ModernBatMovement()
    {
        rigidBat.constraints = RigidbodyConstraints2D.FreezeRotation;

            rigidBat.velocity = new Vector2(Input.GetAxis("Horizontal")*batSpeed,Input.GetAxis("Vertical")*batSpeed);

    }
    //Moves bat in circle around a central object
    void RotatingBatMovement()
    { 
		rigidBat.constraints = RigidbodyConstraints2D.FreezePosition | RigidbodyConstraints2D.FreezeRotation;
 
        rotationAngle += Input.GetAxis("Vertical")*batTurnSpeed;

        if(TurnLimiting)
        {
        rotationAngle = Mathf.Clamp(rotationAngle, turnLimitLeft, turnLimitRight);
        }
		BatParent.transform.localRotation = Quaternion.AngleAxis (rotationAngle, Vector3.forward);
    }
    //Applies different powerups
    void ApplyPowerUp(string powerUpName)
    {
        if(powerUpName == "BiggerBat")
        {
            if (batAnimator.GetBool("isSmaller") == false)
            {
                if (isPowered == false)
                {
                    StopCoroutine("ResetPowerUp");
                    NormalBatCollider.enabled = false;
                    BiggerBatCollider.enabled = true;
                    SmallerBatCollider.enabled = false;
                    batAnimator.SetBool("isBig", true);
                    StartCoroutine("ResetPowerUp");
                    isPowered = true;
                }
                else if (isPowered == true)
                {
                    StopCoroutine("ResetPowerUp");
                    StartCoroutine("ResetPowerUp");
                    isPowered = true;
                }
            }
            else if (batAnimator.GetBool("isSmaller") == true)
            {
                //Resets the bat to original size
                StopCoroutine("ResetPowerUp");
				textAnimator.SetTrigger("Off");
                SmallerBatCollider.enabled = false;
                BiggerBatCollider.enabled = false;
                NormalBatCollider.enabled = true;
                batAnimator.SetBool("isBig", false);
                batAnimator.SetBool("isSmaller", false);
                isPowered = false;
                
            }

            }

        if (powerUpName == "SmallerBat")
        {
            if (batAnimator.GetBool("isBig") == false)
            {
                if (isPowered == false)
                {
                    StopCoroutine("ResetPowerUp");
                    NormalBatCollider.enabled = false;
                    BiggerBatCollider.enabled = false;
                    SmallerBatCollider.enabled = true;
                    batAnimator.SetBool("isSmaller", true);
                    StartCoroutine("ResetPowerUp");
                    isPowered = true;
                
                }
                else if(isPowered == true)
                {
                    StopCoroutine("ResetPowerUp");
                    StartCoroutine("ResetPowerUp");
                    isPowered = true;
                }
            }else if(batAnimator.GetBool("isBig") == true)
            {

                //Resets the bat to original size
                StopCoroutine("ResetPowerUp");
				textAnimator.SetTrigger("Off");
                SmallerBatCollider.enabled = false;
                BiggerBatCollider.enabled = false;
                NormalBatCollider.enabled = true;
                batAnimator.SetBool("isBig", false);
                batAnimator.SetBool("isSmaller", false);
                isPowered = false;
                
            }
        }


    }
    //Resets powerup status after time
    IEnumerator ResetPowerUp()
    {
        int count = ResetPowerUpTime;
        textAnimator.SetTrigger("On");
        for(count = ResetPowerUpTime; count > 0; count--)
        {
            text.text = count.ToString();
            yield return new WaitForSeconds(1f);
        
        }
        batAnimator.SetBool("isBig", false);
        batAnimator.SetBool("isSmaller", false);
        SmallerBatCollider.enabled = false;
        BiggerBatCollider.enabled = false;
        NormalBatCollider.enabled = true;
        isPowered = false;
        textAnimator.SetTrigger("Off");
    }
    //Hit reaction
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "Ball")
        { 
        transform.parent.gameObject.GetComponent<Animator>().SetTrigger("Hit");
        }
    }


}
