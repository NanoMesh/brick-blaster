using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BrickSpawner : MonoBehaviour
{

    //public variables
    public BrickScriptableObjectList[] brickList;
    public GameObject BrickHolderGO;
    public int localLVLNum;
    public bool useLocalLVL;
    public bool LocalDebugSpawn;
    public float SpawnTimer;
    public GameObject[] BrickArray;


	void Start() 
    {
		
    }

	void Update () 
    {
	
	}
    
    public void SpawnBricks()
    {
        if(useLocalLVL)
        {
        GameManager.LVLNumber=localLVLNum;
        }
        if(brickList != null)
        {
            //Reset stats on eachspawn and destroy all current bricks
            if(GameManager.gameMode != GameManager.GameMode.Endless)
            {
            CheckForLivingBricks();
            }
            //Spawn all bricks in scriptable object list
            
            if(LocalDebugSpawn)
            {
                CheckForLivingBricks();
                DebugSpawn();
                
            }
            else
            {
                StartCoroutine(SpawnBricksInList());
            }
        }
        

    }
    
    void CheckForLivingBricks()
    {
        GameManager.ResetStats();
        GameObject[] brickAlive = GameObject.FindGameObjectsWithTag("Brick");
        if(brickAlive != null)
        {
            for(int i=0;i<brickAlive.Length;i++)
            {

                DestroyImmediate(brickAlive[i]);
            }
            Debug.Log("Bricks Resetted");
        }   
    }
    
    IEnumerator SpawnBricksInList()
    {
		if(GameManager.gameMode == GameManager.GameMode.Endless)
			{
				if(GameManager.LVLNumber >= brickList.Length)
				{
					GameManager.LVLNumber = 0;
				}
			}
         foreach(BrickClass brick in brickList[GameManager.LVLNumber].BrickList)
           {
               yield return new WaitForSeconds(SpawnTimer);
                GameObject brickGO;
                GameManager.BrickNum++;
                if (GameManager.gameMode != GameManager.GameMode.Endless)
                {
                    brickGO = brick.BrickPrefab;
                }
                else
                {
                brickGO = RandomBrick();
                }
            
               Quaternion brickRot = Quaternion.Euler(0,0,brick.BrickRotation);
               brickGO = Instantiate (brickGO, new Vector2 (brick.BrickPosX, brick.BrickPosY), brickRot) as GameObject;
               //Assign parent folder in hiearchy
               brickGO.transform.parent = BrickHolderGO.transform;
           }
           Debug.Log("Number of spawned bricks in level:" + GameManager.BrickNum);
    }
    //
    //
    //DEBUG SPAWN
    //
    //
    void DebugSpawn()
    {
         foreach(BrickClass brick in brickList[GameManager.LVLNumber].BrickList)
           {
               GameManager.BrickNum++;
                GameObject brickGO;
                 if (GameManager.gameMode != GameManager.GameMode.Endless)
                {
                    brickGO = brick.BrickPrefab;
                }
                else
                {
                    brickGO = RandomBrick();
                }
            Quaternion brickRot = Quaternion.Euler(0,0,brick.BrickRotation);
               brickGO = Instantiate (brickGO, new Vector2 (brick.BrickPosX, brick.BrickPosY), brickRot) as GameObject;
               //Assign parent folder in hiearchy
               brickGO.transform.parent = BrickHolderGO.transform;
           }
           Debug.Log("Number of spawned bricks in level:" + GameManager.BrickNum);
    }

    GameObject RandomBrick()
    {

        int whichBrick = 1;
        float chance = Random.value;
        //Chance of spawning normal brick
        if (chance < 0.6f)
        {
            whichBrick = 0;
        }
        //Chance of spawning heavy brick
        else if (chance > 0.6f && chance < 0.8f )
        {
            whichBrick = 1;
        }
        //Chance of spawning multiball brick
        else if (chance > .8f && chance < 0.82f)
        {
            whichBrick = 2;
        }
        //Chance of spawning extralife brick
        else if (chance > .82f && chance < 0.85f)
        {
            whichBrick = 3;
        }
        //Chance of spawning longer brick
        else if (chance > .85f && chance < .9f)
        {
            whichBrick = 4;
        }
        //Chance of spawning smaller brick
        else if (chance > .9f && chance < .95f)
        {
            whichBrick = 5;
        }
        else
        {
            whichBrick = 0;
        }
        //Debug.Log("Current % of brick spawning is" + Mathf.RoundToInt(chance * 100) + " " + chance);
        return BrickArray[whichBrick];
    }


}
