using UnityEngine;
using System.Collections;

public class BallShooter : MonoBehaviour 
{
    //public varibales   
    public float turnSpeed;
    public float turnLimitLeft;
    public float turnLimitRight;
    public float rotationAngle;
    public Rigidbody2D ballPrefab;
	private Animator shooterAnim;

    
	void Start () 
    {
		shooterAnim = gameObject.GetComponent<Animator> ();
	}
	
	void Update () 
    {        

        if(GameManager.gameState == GameManager.GameState.Firing)
        {
			shooterAnim.SetBool ("ShooterOn", true);
            Rotate();
            Shoot();
		}else
		{
			shooterAnim.SetBool ("ShooterOn", false);
		}
        
	}
    
    void Rotate()
    {
        //Limited rotating
        rotationAngle += Input.GetAxis("Vertical")*Time.deltaTime*turnSpeed;
        rotationAngle = Mathf.Clamp(rotationAngle, turnLimitLeft, turnLimitRight);
        transform.localRotation = Quaternion.AngleAxis(rotationAngle, Vector3.forward);
    }
    
    
    void Shoot()
    {
     //Ball Shooting
        if(GameManager.NumberOfLives > 0)
        {
            if(Input.GetButtonDown("Fire1"))
                {
                   Debug.Log("Ball Shot");
                   GameManager.NumberOfBalls++;
                   Rigidbody2D clone = Instantiate(ballPrefab,transform.position,transform.rotation) as Rigidbody2D;
                   clone.AddForce(gameObject.transform.right);
                   GameManager.gameState = GameManager.GameState.Bouncing;
                }

        }   
    }
    
}
